/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package productmanagerapp;

/**
 *
 * @author medel
 */
public class Store {

    //Atributes
    private Product[] products;
    private int maxElements;
    private int numElements;

    //Constructors
    public Store(int maxElements) {
        this.maxElements = maxElements;
        this.numElements = 0;
        this.products = new Product[maxElements];
    }

    //setters and getters
    public Product[] getProducts() {
        return products;
    }

    public void setProducts(Product[] products) {
        this.products = products;
    }

    public int getMaxElements() {
        return maxElements;
    }

    public void setMaxElements(int maxElements) {
        this.maxElements = maxElements;
    }

    public int getNumElements() {
        return numElements;
    }

    /**
     * Adds a product to the array of products if there is space
     *
     * @param p
     * @return 0 if succes and -1 otherwise
     */
    public int add(Product p) {
        int result = -1;
        if (numElements < maxElements) {
            products[numElements] = p;
            numElements++;
            result = 0;
        } else {
            result = -1;
        }
        return result;
    }

    public Product findByCode(String code) {
        Product product = null;
        for (int i = 0; i < numElements && product == null; i++) {
            if (code.equals(products[i].getCode())) {
                product = products[i];
            }
        }
        return product;
    }

    public Store findByName(String name) {
        int count = 0;
        for (int i = 0; i < numElements; i++) {
            if (name.equals(products[i].getName())) {
                count++;
            }
        }
        Store store = new Store(count);
        for (int i = 0; i < numElements; i++) {
            if (name.equals(products[i].getName())) {
                store.add(products[i]);
            }
        }
        return store;
    }

    public Product find(Product p) {
        boolean found = false;
        for (Product product : products) {
            if (p.equals(product)){
                p = product;
                found = true;
            }
        }
        if (found == false) p = null;
        return p;
    }
}
