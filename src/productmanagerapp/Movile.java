/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package productmanagerapp;

/**
 *
 * @author medel
 */
public class Movile extends Product{
    private int inches;
    
    //constructors
    public Movile(String code, String name, double price, int inches){
        super (code,name,price);
        this.inches = inches;
    }
    public Movile(String code){
        super(code);
    }
    public Movile(Movile other){
        super(other.code,other.name,other.price);
        this.inches = other.inches;
    }
    
    //getters and setters

    public int getInches() {
        return inches;
    }

    public void setInches(int inches) {
        this.inches = inches;
    }
    
    @Override
    public String toString(){
        StringBuilder sb = new StringBuilder();
        sb.append("\"Movile\":{");
        sb.append("\"inches\"=");
        sb.append(inches);
        sb.append("; ");
        sb.append(super.toString());
        sb.append("}");
        return sb.toString();
    }
    
    
}
