/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package productmanagerapp;

/**
 *
 * @author medel
 */
public class PC extends Product {

    private int ram;
    private int hd;

    //constructors
    public PC(String code, String name, double price, int ram, int hd) {
        super(code, name, price);
        this.ram = ram;
        this.hd = hd;
    }

    public PC(String code) {
        super(code);
    }

    public PC(PC other) {
        super(other.code, other.name, other.price);
        this.ram = other.ram;
        this.hd = other.hd;
    }

    //getters and setters
    public int getRam() {
        return ram;
    }

    public void setRam(int ram) {
        this.ram = ram;
    }

    public int getHd() {
        return hd;
    }

    public void setHd(int hd) {
        this.hd = hd;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("\"PC\":{");
        sb.append("\"ram\"=");
        sb.append(ram);
        sb.append(", ");
        sb.append("\"hd\"=");
        sb.append(hd);
        sb.append(", ");
        sb.append(super.toString());
        sb.append("}");
        return sb.toString();
    }
}
