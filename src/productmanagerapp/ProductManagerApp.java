/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package productmanagerapp;

import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

public class ProductManagerApp {

    Store myStore;

    public static void main(String[] args) {

        ProductManagerApp myApp = new ProductManagerApp();
        myApp.run();
    }

    /*
    This method runs the app
     */
    private void run() {
        int option = 0;
        myStore = new Store(10);
        loadData();
        do {
            option = showMenu();
            switch (option) {
                case 1:
                    System.out.println("List all products");
                    listAllProducts();
                    break;
                case 2:
                    System.out.println("Find product by code");
                    findProductByCode();
                    break;
                case 3:
                    System.out.println("Find product by name");
                    findProductByName();
                    break;
                case 4:
                    System.out.println("Add new product");
                    addNewProductStore();
                    break;
                case 5:
                    System.out.println("Modify product");
                    modifyProduct();
                    break;
            }

        } while (option != 0);
    }

    /**
     * Shows the menu to the user and asks for an option
     *
     * @return the selected option or -1 in case of error
     */
    private int showMenu() {
        int option = -1;

        System.out.println("0.-Exit");
        System.out.println("1.-List all products");
        System.out.println("2.-Find product by code");
        System.out.println("3.-Find prodcut by name");
        System.out.println("4.-Add new product");
        System.out.println("5.-Modify product");

        System.out.print("\nChoose an option: ");

        try {
            Scanner myScan = new Scanner(System.in);
            option = myScan.nextInt();
            myScan.nextLine();

        } catch (Exception e) {
            System.out.println("Data error");
        }

        return option;
    }

    private void loadData() {
        Product p1 = new Product("111", "Keyboard Razer", 40);
        myStore.add(p1);
        myStore.add(new Product("222", "Monitor hp", 200));
        myStore.add(new Product("333", "Mouse Razer", 35));
        Product p4 = new TV("444", "TV Samsung", 480, 50);
        myStore.add(p4);
        myStore.add(new TV("555", "TV Samsung", 600, 50));
        myStore.add(new Fridge("666", "Fridge Bosch", 599.99, 3));
        myStore.add(new Fridge("777", "Fridge Daewoo", 449.99, 2));
        myStore.add(new PC("888", "PC lenovo", 895.95, 8, 1000));
        myStore.add(new PC("999", "PC hp pavilion", 699.99, 8, 1000));
    }

    private void listAllProducts() {
        for (int i = 0; i < myStore.getNumElements(); i++) {
            System.out.println(myStore.getProducts()[i].toString());
        }
    }

    private void findProductByCode() {
        String code;
        Scanner sc = new Scanner(System.in);
        System.out.println("Insert code: ");
        code = sc.nextLine();

        if (myStore.findByCode(code) == null) {
            System.out.println("Product not found");
        } else {
            System.out.println(myStore.findByCode(code).toString());
        }
    }

    private void findProductByName() {
        String name;
        Scanner sc = new Scanner(System.in);
        System.out.println("Insert name: ");
        name = sc.nextLine();
        Store s = myStore.findByName(name);
        if (s.getNumElements() == 0) {
            System.out.println("Product not found");
        } else {
            for (int i = 0; i < s.getNumElements(); i++) {
                System.out.println(s.getProducts()[i].toString());
            }
        }
    }

    private void addNewProductStore() {
        Product p = new Product();
        String valid_options[] = {"1", "2", "3", "4"};
        List<String> list_valid_options = Arrays.asList(valid_options);
        int option;
        Scanner sc = new Scanner(System.in);
        System.out.println("Code of the product:");
        String code = sc.nextLine();
        System.out.println("Name of the product:");
        String name = sc.nextLine();
        System.out.println("Price:");
        int price = sc.nextInt();
        sc.nextLine();

        do {
            option = kindOfProduct();
            switch (option) {
                case 1:
                    p = new Product(code, name, price);
                    break;
                case 2:
                    System.out.println("Introduce inches");
                    int inches = sc.nextInt();
                    sc.nextLine();
                    p = new TV(code, name, price, inches);
                    break;
                case 3:
                    System.out.println("Introduce number of doors");
                    int doors = sc.nextInt();
                    sc.nextLine();
                    p = new Fridge(code, name, price, doors);
                    break;
                case 4:
                    System.out.println("Introduce RAM (in GB)");
                    int ram = sc.nextInt();
                    System.out.println("Introduce Hard Disk (in MB)");
                    int hd = sc.nextInt();
                    sc.nextLine();
                    p = new PC(code, name, price, ram, hd);
                    break;
                default:
                    System.out.println("Option not in list, try another time");
                    break;
            }
        } while (!list_valid_options.contains(Integer.toString(option)));

        if (myStore.find(p) != null) {
            System.out.println("cannot add this product, already exist");
        } else if (myStore.add(p) == -1) {
            System.out.println("can not add product, exceeded the maximus space of the store");
        } else {
            System.out.println("product added successfully");
        }
    }

    private int kindOfProduct() {
        int option = -1;

        System.out.println("1.-Normal product");
        System.out.println("2.-TV");
        System.out.println("3.-Fridge");
        System.out.println("4.-PC");

        System.out.print("\nChoose an option: ");

        try {
            Scanner myScan = new Scanner(System.in);
            option = myScan.nextInt();
            myScan.nextLine();

        } catch (Exception e) {
            System.out.println("Data error");
        }

        return option;
    }

    private void modifyProduct() {
        String code;
        Scanner sc = new Scanner(System.in);
        int field;

        listAllProducts();
        System.out.println("Insert the product code you want to modify");
        code = sc.nextLine();
        if (myStore.findByCode(code) == null) {
            System.out.println("Product not found");
        } else {
            Product p = myStore.findByCode(code);
            System.out.println(p.toString());
            System.out.println("Which field do u want to modify?");
            field_menu(p);
            field = select_field(p);
            new_field(field, p);
        }
    }

    private void field_menu(Product p) {
        System.out.println("1.-Name");
        System.out.println("2.-Price");
        if (p.getClass() == TV.class) {
            System.out.println("3.-Inches");
        } else if (p.getClass() == Fridge.class) {
            System.out.println("3.-Doors");
        } else {
            System.out.println("3.-RAM");
            System.out.println("4.-HD");
        }
    }

    private int select_field(Product p) {
        int option = -1;
        String valid_options[];
        if (p.getClass() == Product.class) {
            valid_options = new String[]{"1", "2"};
        } else if (p.getClass() == TV.class) {
            valid_options = new String[]{"1", "2", "3"};
        } else if (p.getClass() == Fridge.class) {
            valid_options = new String[]{"1", "2", "3"};
        } else {
            valid_options = new String[]{"1", "2", "3", "4"};
        }
        List<String> list_valid_options = Arrays.asList(valid_options);

        do {
            try {
                Scanner myScan = new Scanner(System.in);
                option = myScan.nextInt();
                myScan.nextLine();

            } catch (Exception e) {
                System.out.println("Data error");
            }
        } while (!list_valid_options.contains(Integer.toString(option)));

        return option;
    }

    private void new_field(int f, Product p) {
        Scanner myScan = new Scanner(System.in);
        if (f == 1) {
            System.out.println("New name:");
            String new_name = myScan.nextLine();
            p.setName(new_name);
        } else if (f == 2) {
            System.out.println("New price:");
            int new_price = myScan.nextInt();
            p.setPrice(new_price);
        } else if (p.getClass() == TV.class) {
            System.out.println("New inches:");
            int new_inches = myScan.nextInt();
            TV tv = (TV) p;
            tv.setInches(new_inches);
        } else if (p.getClass() == Fridge.class) {
            System.out.println("New number of doors:");
            int new_doors = myScan.nextInt();
            Fridge fridge = (Fridge) p;
            fridge.setNumDoors(new_doors);
        } else {
            if (f == 3) {
                System.out.println("New RAM:");
                int new_ram = myScan.nextInt();
                PC pc = (PC) p;
                pc.setRam(new_ram);
            } else {
                System.out.println("New HD:");
                int new_hd = myScan.nextInt();
                PC pc = (PC) p;
                pc.setHd(new_hd);
            }
        }
    }
}
